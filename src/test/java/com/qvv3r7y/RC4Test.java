package com.qvv3r7y;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RC4Test {
    RC4 rc4Decrypt;
    RC4 rc4Encrypt;

    @Test
    void encrypt() {
        rc4Encrypt = new RC4("test");
        String text = "Hello world";
        assertArrayEquals(rc4Encrypt.encrypt(text.getBytes(StandardCharsets.UTF_8)), new byte[]{-26, -22, 75, 125, -113, 10, 10, 74, -92, 34, 57});
    }

    @Test
    void decrypt() {
        rc4Decrypt = new RC4("test");
        byte[] cipher = new byte[]{-26, -22, 75, 125, -113, 10, 10, 74, -92, 34, 57};
        String result = new String(rc4Decrypt.decrypt(cipher));
        assertEquals("Hello world", result);
    }
}