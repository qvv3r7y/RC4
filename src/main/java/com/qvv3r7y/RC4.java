package com.qvv3r7y;

public class RC4 {
    private static final int S_SIZE = 256;

    private final byte[] S = new byte[S_SIZE];
    private int k = 0;
    private int q = 0;

    public RC4(String key) {
        this(key.getBytes());
    }

    public RC4(final byte[] key) {
        if (key == null || key.length == 0) {
            throw new NullPointerException();
        }
        initS(key);
    }

    public byte[] encrypt(final byte[] openData) {
        byte[] cipher = new byte[openData.length];
        for (int i = 0; i < openData.length; i++) {
            cipher[i] = (byte) (openData[i] ^ generateNextKeyByte());
        }
        return cipher;
    }

    public byte[] decrypt(final byte[] cipher) {
        return encrypt(cipher);
    }

    private void initS(byte[] key) {
        for (int i = 0; i < S_SIZE; i++) {
            S[i] = (byte) i;
        }
        int j = 0;
        for (int i = 0; i < S_SIZE; i++) {
            j = (j + S[i] + key[i % key.length]) & 0xFF;
            swapByteArray(S, i, j);
        }
    }

    private byte generateNextKeyByte() {
        k = (k + 1) & 0xFF;
        q = (q + S[k]) & 0xFF;

        swapByteArray(S, k, q);
        int index = (S[k] + S[q]) & 0xFF;
        return S[index];
    }

    private void swapByteArray(byte[] array, int i, int j) {
        byte tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
}
